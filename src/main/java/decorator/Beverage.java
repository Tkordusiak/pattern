package decorator;

public abstract class Beverage {

    public String  getDescription(){
        return "Unknow description";
    }

    public abstract double getCost();
}
