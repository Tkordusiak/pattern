package decorator;

public class Main {
    public static void main(String[] args) {
        CarmelDecorator espressoWithMil = new CarmelDecorator(
                new MilkDecorator(
                        new Espresso()));

        System.out.println(espressoWithMil.getCost());
        System.out.println(espressoWithMil.getDescription());
    }
}
