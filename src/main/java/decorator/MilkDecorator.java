package decorator;

public class MilkDecorator extends BeverageDecorator {

    public static final double MILK_PRICE = 0.5;

    public MilkDecorator(Beverage beverage) {
        super(beverage);
    }

    @Override
    public double getCost() {
        double costOfBeverage = super.getCost();
        return costOfBeverage + MILK_PRICE;
    }

    @Override
    public String getDescription() {
        String descriptionOfBeverage = super.getDescription();
        return descriptionOfBeverage + " with milk ";
    }
}
