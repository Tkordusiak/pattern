package decorator;

public class CarmelDecorator extends BeverageDecorator {

    public static final double CARMEL_PRICE = 1.5;

    public CarmelDecorator(Beverage beverage) {
        super(beverage);
    }

    @Override
    public double getCost() {
        double costOfBeverage = super.getCost();
        return costOfBeverage + CARMEL_PRICE;
    }

    @Override
    public String getDescription() {
        String descriptionOf = super.getDescription();
        return descriptionOf + " with carmel ";
    }
}
