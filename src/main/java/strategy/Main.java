package strategy;

public class Main {
    public static void main(String[] args) {
        Payment payCard = new PayCard();

        OnlineShop shop = new OnlineShop(payCard);
        shop.paymentMethod();
        shop.setPayment(new PayTransfer());
        shop.paymentMethod();


    }
}
