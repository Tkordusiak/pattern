package strategy;

public class PayPal implements Payment {

    @Override
    public void payMethod() {
        System.out.println("płatnośc PayPal");
    }
}
