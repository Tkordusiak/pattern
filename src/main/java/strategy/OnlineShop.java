package strategy;

public class OnlineShop {

    private Payment payment;

    public OnlineShop(Payment payment) {
        this.payment = payment;
    }

    public void paymentMethod(){
        System.out.println("wybrałes płatność: ");
        payment.payMethod();
    }

    public OnlineShop setPayment(Payment payment) {
        this.payment = payment;
        return this;
    }
}
