package strategy;

public interface Payment {

    void payMethod();
}
