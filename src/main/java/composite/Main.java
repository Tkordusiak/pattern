package composite;

public class Main {
    public static void main(String[] args) {

        MenuComponent breakfast  = new Menu("Menu śniadaniowe", "do 11:00");
        MenuComponent diner = new Menu("menu obiadowe", "lunch");
        MenuComponent alcochol = new Menu("menu alkocholowe", "%");

        MenuComponent allMenu = new Menu("menu ", "all menu");

        allMenu.add(breakfast);
        allMenu.add(diner);
        allMenu.add(alcochol);

        MenuComponent beer = new MenuItem(4.0, "Pinta", "stout", false);
        alcochol.add(beer);
        MenuComponent eggs = new MenuItem(2.0, "jajko", "kurze", false);
        breakfast.add(eggs);

        Waitress waitress = new Waitress(allMenu);
        waitress.printBreakfast();

    }
}
