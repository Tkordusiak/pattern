package composite;

public class MenuItem extends MenuComponent {

    private double price;
    private String name;
    private String description;
    private Boolean vege;

    public MenuItem(double price, String name, String description, Boolean vege) {
        this.price = price;
        this.name = name;
        this.description = description;
        this.vege = vege;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public boolean isVege() {
        return vege;
    }

    @Override
    public String print() {
        return
                "\n"+name +
                (isVege() ? "(v)" : " ") +
                description + " "+price + "\n" +
                "--------------------"+
                "\n";
    }
}
