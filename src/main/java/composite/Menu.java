package composite;


import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

// clasa Node, służy do przechopwywania całego menau
// np. menu sniadaniowe które mędzie mieć menuItem.
public class Menu extends MenuComponent {

    private String name;
    private String description;
    List<MenuComponent> menuComponents = new ArrayList<>();

    public Menu(String name, String descripton) {
        this.name = name;
        this.description = descripton;
    }

    @Override
    public void add(MenuComponent component) {
        menuComponents.add(component);
    }

    @Override
    public void remove(MenuComponent component) {
        menuComponents.remove(component);
    }

    @Override
    public String getName() {
        return name;
    }


    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String print() {
        return
                "\n" +getName() + " " +
                getDescription() + "\n" +
                "--------------------" +
                menuComponents.stream()
                        .map(MenuComponent::print)
                        .collect(Collectors.joining());
    }

    @Override
    public Stream<MenuComponent> createComponentStream() {
        return menuComponents.stream();

    }
}
