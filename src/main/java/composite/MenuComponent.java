package composite;

import java.util.stream.Stream;

public abstract class MenuComponent {

    public String getName(){
        throw new UnsupportedOperationException();
    }
    public String getDescription(){
        throw new UnsupportedOperationException();
    }
    public double getPrice(){
        throw new UnsupportedOperationException();
    }

   public void add(MenuComponent component){
       throw new UnsupportedOperationException();
   }

   public void remove(MenuComponent component){
       throw new UnsupportedOperationException();
   }

   public boolean isVege(){
        throw new UnsupportedOperationException();
   }

   public String print(){
        throw new UnsupportedOperationException();
   }

   public Stream<MenuComponent> createComponentStream(){
        throw new UnsupportedOperationException();
   }
}
