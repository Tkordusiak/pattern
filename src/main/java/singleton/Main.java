package singleton;

public class Main {
    public static void main(String[] args) {

        Singleton singleton = Singleton.getInstance();
        singleton.incrementValue();
        System.out.println(singleton.getValue());

        singleton = Singleton.getInstance();
        singleton.incrementValue();
        System.out.println(singleton.getValue());
    }
}
