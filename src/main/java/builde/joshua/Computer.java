package builde.joshua;

import java.time.LocalDate;

public class Computer {

    private double ghz;
    private int ram;
    private double screamInches;
    private LocalDate productionDate;
    private double weight;
    private String system;

    public double getGhz() {
        return ghz;
    }

    public void setGhz(double ghz) {
        this.ghz = ghz;
    }

    public int getRam() {
        return ram;
    }

    public void setRam(int ram) {
        this.ram = ram;
    }

    public double getScreamInches() {
        return screamInches;
    }

    public void setScreamInches(double screamInches) {
        this.screamInches = screamInches;
    }

    public LocalDate getProductionDate() {
        return productionDate;
    }

    public void setProductionDate(LocalDate productionDate) {
        this.productionDate = productionDate;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public String getSystem() {
        return system;
    }

    public void setSystem(String system) {
        this.system = system;
    }

    @Override
    public String toString() {
        return "Computer{" +
                "ghz=" + ghz +
                ", ram=" + ram +
                ", screamInches=" + screamInches +
                ", productionDate=" + productionDate +
                ", weight=" + weight +
                ", system='" + system + '\'' +
                '}';
    }
}
