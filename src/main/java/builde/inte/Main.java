package builde.inte;

import java.time.LocalDate;

public class Main {

    public static void main(String[] args) {
        Computer computer = new ComputerBuilderImpl()
                .ram(2048)
                .ghz(2.4)
                .screamInches(25.0)
                .weight(1.2)
                .system("iOS")
                .productionDate(LocalDate.of(2019, 05, 02))
                .build();
        System.out.println(computer);
    }


}
