package builde.inte;

import java.time.LocalDate;

public class ComputerBuilderImpl implements ComuterBuilder {

    private Computer computer;

    public ComputerBuilderImpl() {
        this.computer = new Computer();
    }

    @Override
    public Computer build() {
        return computer;
    }

    @Override
    public ComuterBuilder ghz(double ghz) {
        computer.setGhz(ghz);
        return this;
    }

    @Override
    public ComuterBuilder ram(int ram) {
        computer.setRam(ram);
        return this;
    }

    @Override
    public ComuterBuilder screamInches(double screamInches) {
        computer.setScreamInches(screamInches);
        return this;
    }

    @Override
    public ComuterBuilder productionDate(LocalDate productionDate) {
        computer.setProductionDate(productionDate);
        return this;
    }

    @Override
    public ComuterBuilder weight(double weight) {
        computer.setWeight(weight);
        return this;
    }

    @Override
    public ComuterBuilder system(String system) {
        computer.setSystem(system);
        return this;
    }
}
