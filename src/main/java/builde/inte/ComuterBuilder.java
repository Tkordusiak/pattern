package builde.inte;

import java.time.LocalDate;

public interface ComuterBuilder {

    Computer build();

    ComuterBuilder ghz(double ghz);
    ComuterBuilder ram(int ram);
    ComuterBuilder screamInches(double screamInches);
    ComuterBuilder productionDate(LocalDate productionDate);
    ComuterBuilder weight(double weight);
    ComuterBuilder system(String system);

}
